function varargout = CerebtDCS(varargin)
% CEREBTDCS MATLAB code for CerebtDCS.fig
%      CEREBTDCS, by itself, creates a new CEREBTDCS or raises the existing
%      singleton*.
%
%      H = CEREBTDCS returns the handle to a new CEREBTDCS or the handle to
%      the existing singleton*.
%
%      CEREBTDCS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CEREBTDCS.M with the given input arguments.
%
%      CEREBTDCS('Property','Value',...) creates a new CEREBTDCS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CerebtDCS_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CerebtDCS_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CerebtDCS

% Last Modified by GUIDE v2.5 23-Aug-2016 15:09:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CerebtDCS_OpeningFcn, ...
                   'gui_OutputFcn',  @CerebtDCS_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CerebtDCS is made visible.
function CerebtDCS_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CerebtDCS (see VARARGIN)

% Choose default command line output for CerebtDCS
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
movegui(gcf,'center')
cd(fileparts(mfilename('fullpath')));
setPathForHomebrew;
if ~exist('Screen') %#ok
    uiwait(msgbox('Psychtoolbox is not installed, or not available to matlab''s search path'));
    web('http://psychtoolbox.org/download/','-browser');
    error('Psychtoolbox is not installed, or not available to matlab''s search path');
end
if ~checkForBrew 
    uiwait(msgbox('You are missing some software. Opening your browser now so that you can download it. The Terminal App has also been opened','Download software'));
    system('open -a Terminal');
    web('http://brew.sh/','-browser');
    error('Homebrew is not installed. Video recording needs it.'); 
end
if ~checkForffmpeg
    clipboard('copy', 'brew install ffmpeg');
    uiwait(msgbox('A command has been copied. Use CTRL + V to paste it in Terminal, then press enter. Click OK to open Terminal'));
    system('open -a Terminal');
    error('ffmpeg is not installed. Video recording needs it.\n\nPaste this in terminal app: brew install ffmpeg');
end
if ~checkForPidof
    clipboard('copy', 'brew install pidof');
    uiwait(msgbox('A command has been copied. Use CTRL + V to paste it in Terminal, then press enter. Click OK to open Terminal'));
    system('open -a Terminal');
    error('pidof is not installed. Video recording needs it.\n\nPaste this in terminal app: brew install pidof');
end
if ~checkForGstreamer
    uiwait(msgbox('Gstreamer is not installed. Opening web browser so you can download the latest version'));
    web('https://gstreamer.freedesktop.org/data/pkg/osx/','-browser');
end
% UIWAIT makes CerebtDCS wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = CerebtDCS_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pntBtn.
function pntBtn_Callback(hObject, eventdata, handles)
% hObject    handle to pntBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp('starting PNT')
run(fullfile(fileparts(mfilename('fullpath')),'PNT','PNT'));
figure(gcf)
movegui(gcf,'center')



% --- Executes on button press in naming80Btn.
function naming80Btn_Callback(hObject, eventdata, handles)
% hObject    handle to naming80Btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp('starting naming80')
run(fullfile(fileparts(mfilename('fullpath')),'naming80','naming80'));
figure(gcf)
movegui(gcf,'center')


% --- Executes on button press in screeningBtn.
function screeningBtn_Callback(hObject, eventdata, handles)
% hObject    handle to screeningBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp('starting treatment screening')
run(fullfile(fileparts(mfilename('fullpath')),'tx','screening'));
figure(gcf)
movegui(gcf,'center')


% --- Executes on button press in treatmentBtn.
function treatmentBtn_Callback(hObject, eventdata, handles)
% hObject    handle to treatmentBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp('starting treatment')
run(fullfile(fileparts(mfilename('fullpath')),'tx','tx'));
figure(gcf)
movegui(gcf,'center')


% --- Executes on button press in camBtn.
function camBtn_Callback(hObject, eventdata, handles)
% hObject    handle to camBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp('starting camera')
run(fullfile(fileparts(mfilename('fullpath')),'MatCam','MatCam'));
figure(gcf)
movegui(gcf,'center')


% --- Executes on button press in fmriBtn.
function fmriBtn_Callback(hObject, eventdata, handles)
% hObject    handle to fmriBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
disp('starting fmri naming task')
run(fullfile(fileparts(mfilename('fullpath')),'fmriNaming','fmriNaming'));
figure(gcf)
movegui(gcf,'center')



function isInstalled = checkForBrew
[~, r] = system('which brew');
if isempty(r)
    isInstalled = false;
else
    isInstalled = true;
end

function isInstalled = checkForffmpeg
[~, r] = system('which ffmpeg');
if isempty(r)
    isInstalled = false;
else
    isInstalled = true;
end


function isInstalled = checkForPidof
[~, r] = system('which pidof');
if isempty(r)
    isInstalled = false;
else
    isInstalled = true;
end

function setPathForHomebrew
PATH = getenv('PATH');
setenv('PATH', [PATH ':/usr/local/bin']);
PATH = getenv('PATH');


function isInstalled = checkForGstreamer
isInstalled = exist('/Library/Frameworks/GStreamer.framework','dir');
if isInstalled > 0
    isInstalled = true;
end
