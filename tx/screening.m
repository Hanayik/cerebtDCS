function catcherror = screening
% This script will present a picture then a vid to a subject.
% The subject will then decide if the picture matched the word from the
% video.
catcherror = 0;
[subj, sess, picdur, btwTime, respTime ] = getSessionInfo; if isempty(subj); return; end


% ------------------------- setup vars ------------------------------------
% -------------------------------------------------------------------------
T = table;
t = T;
numtrials = 40; % 6.75s/trial * 40 trials
veryPreciseTimingNeeded = false;
background = [0.5 0.5 0.5];
textColor = [0 0 0];
KbName('UnifyKeyNames');
[viddir, picdir, datadir, mpath] = getPaths;
w = load(fullfile(mpath, 'words.mat')); %was "wordsrevised.mat", changed to make less cryptic
words = w.words;
subdatadir = fullfile(datadir, subj, mfilename); 
if ~exist(subdatadir,'dir'); mkdir(subdatadir); end;
rand('twister',sum(100*clock));%#ok
if veryPreciseTimingNeeded
    Screen('Preference', 'SkipSyncTests', 0); %#ok
else
    Screen('Preference', 'SkipSyncTests',2);
    Screen('Preference', 'VisualDebugLevel', 1);
end
datestring = getDateAndTime;
datafile = fullfile(subdatadir,sprintf('%s_%s_%s_%s.csv',subj, sess, mfilename, datestring)); % make data file name for saving
if exist(datafile,'file')
    x = readtable(datafile);
    formatStr = repmat('%s',1,size(x,2));
    clear x;
    T = readtable(datafile,'Format',formatStr);
    %c = table2cell(T);
    numtrials = 400-size(T,1);
end
feedbacktime = 0.5;
fixationTime = 0.25; 
PsychDefaultSetupPlus(3); % set up PTB with some standard values
params = PsychSetupParams(0,1, background, textColor); % set some standard parameters for this session
params.vidRect = [0 0 params.rect(3)/2 params.rect(4)/2];
params.vidRect = CenterRectOnPoint(params.vidRect,params.Xc,params.Yc);
vidKeys = {'1','2','1!','2@','escape'};
respKeys = vidKeys;
instructKeys = {'escape','space','1','1!'};
respPrompt = '';
instruct = 'You will see a picture\n\nthen a video. Please choose\n\nif they match or do not match.\n\nGreen = Match    Red = No Match\n\nPress the Green button to start.';

try
    startTime = ShowInstructions(params, instruct,instructKeys);
    for k = 1:numtrials
        % query stimuli for this trial
        trialStartTime = GetSecs;
        [targetWord, compWord, correctResp] = getTargetWordAndCompWord(words);
        foilType = getFoilType(compWord);
        picname = fullfile(picdir, [targetWord '.jpg']);
        vidname = fullfile(viddir, [compWord '.mp4']);
        fixOnset = showFixationScreen(params, fixationTime, {'escape'}); 
        fixOnset = fixOnset - startTime;
        imgOnset = showPicture(params, picname, picdur);
        imgOnset = imgOnset - startTime;
        WaitSecs(btwTime);
        [rt, responseKey, videoStartTime] = PlayVideo(params, vidname, [], vidKeys);
        videoOnsetTime = videoStartTime - startTime;
        if isempty(rt)
            [responseKey, timeOfBtnPress] = showResponseScreen(params, respPrompt, respTime, respKeys);
            rt = timeOfBtnPress - videoStartTime;
        end
        isCorrect = checkIfTrialIsCorrect(responseKey,correctResp);
        showFeedbackScreen(params, isCorrect,feedbacktime);
        Screen('Close');
        trialOffTime = WaitSecs('UntilTime',trialStartTime + 6.75);
        trialDuration = trialOffTime - trialStartTime;
        %if a no response, then make sure we change the -1 to a 0 before
        %saving, since a no response is wrong, we just need -1 for
        %displaying "no response" to the subject
        if isCorrect < 0
            isCorrect = 0;
        end
        % save vars to table
        %varNames = {'subj','sess','picdur','targetWord','compWord','correctResp','fixOnset','imgOnset','rt','responseKey','videoOnsetTime','isCorrect','trialDuration'};
        %c = [c; {subj,sess,picdur,targetWord,compWord,correctResp,fixOnset,imgOnset,rt,responseKey,videoOnsetTime,isCorrect,trialDuration}];
        t.subj = {subj};
        t.session = {sess};
        t.picDur = picdur;
        t.targetWord = {targetWord};
        t.comparisonWord = {compWord};
        t.foilType = {foilType};
        t.correctResp = {correctResp};
        t.fixationOnset = {fixOnset};
        t.imgOnset = {imgOnset};
        t.RT = {rt};
        t.response = {responseKey};
        t.vidOnset = {videoOnsetTime};
        t.accuracy = {isCorrect};
        t.trialDuration = {trialDuration};
        T = [T; t];
        writetable(T,datafile);
        accuracy(k,1) = isCorrect;
    end %numtrials

    % save percent and present it to clinician
    acc = mean(accuracy)*100;
    fprintf('*************************\n')
    fprintf('\n')
    fprintf('Accuracy = %3.2f percent\n',acc)
    fprintf('\n')
    fprintf('*************************\n')
    Screen('CloseAll');
    Accmessage = sprintf('Accuracy: %3.2f percent\n\n',acc);
    uiwait(msgbox(Accmessage));
    CleanUp;
catch catcherror
writetable(T, datafile)
CleanUp;
end

end


function [subj, sess, picdur, btwTime, respTime] = getSessionInfo
subj = [];
sess = [];
picdur = [];
btwTime = [];
respTime = [];
prompt={'Participant: ','Session: ', 'Picture duration: ', 'pic/vid gap: ', 'time to resp: '};
name='Treatment Info';
numlines=1;
defaultanswer={'0', '0', '1.5', '0', '1.5'};
answer=inputdlg(prompt,name,numlines,defaultanswer);
if isempty(answer); return; end
subj = answer{1}; % string
sess = answer{2}; % string
picdur = str2num(answer{3});  %#ok
btwTime = str2num(answer{4});  %#ok
respTime = str2num(answer{5});  %#ok
end 


function [viddir, picdir, datadir, mpath] = getPaths
mpath = fileparts(mfilename('fullpath'));
viddir = fullfile(mpath, 'txvids');
picdir = fullfile(mpath, 'txpics');
datadir = fullfile(mpath, 'data');
end


function datestring = getDateAndTime()
d = fix(clock);
datestring=sprintf('Y%04d_M%02d_D%02d',...
    d(1),...
    d(2),...
    d(3));
end


function params = PsychSetupParams(doAlphaBlending,doMultiSample, background, textColor)
%sets up some normal values used in experiments such as a gray background
%and Arial font, and a large text size, etc...
%saves all relevant screen info to the 'params' structure so that the
%entire structure can be passed in and out of functions, rather than
%zillions of variables. Also makes it expandable.
%
% History:
% 29-May-2015   th     made initial version of the function
if nargin < 3
    background = [1 1 1];
end
if nargin < 4
    textColor = [0 0 0];
end

global psych_default_colormode;
%make params structure
params = struct;
%set some defualt, common colors
params.colors.white = [1 1 1];
params.colors.black = [0 0 0];
params.colors.gray = [0.5 0.5 0.5];
params.colors.red = [1 0 0];
params.colors.green = [0 1 0];
%check if using normalized color values or not
if psych_default_colormode == 0
    params.colors.white = [255 255 255];
    params.colors.gray = [128 128 128];
end
%choose max screen number (will be the external monitor if connected)
params.screen = max(Screen('Screens'));
params.font = 'Arial'; %set the global font for PTB to use
params.tsize = 24; %set text size
params.TextColor = textColor; %set global text color
%set the background color of the screen (defaults to gray)
params.background = background;
params.multiSample = [];
if doMultiSample
    params.multiSample = 4;%set to a value greater than 0 if you want super sampling
end
%open the PTB window
[params.win, params.rect] = PsychImaging('OpenWindow', params.screen, params.background,[],[],[],[],params.multiSample);
%get screen width and height
[params.maxXpixels, params.maxYpixels] = Screen('WindowSize', params.win);
if doAlphaBlending
    %Set blend function for alpha blending
    Screen('BlendFunction', params.win, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
end
%find center of screen
[params.Xc,params.Yc] = RectCenter([0 0 params.maxXpixels params.maxYpixels]);
params.fbRect = CenterRectOnPoint([0 0 50 50],params.Xc,params.Yc);
%now that the window pointer exists, set some values from earlier
Screen('TextSize', params.win, params.tsize);
Screen('TextFont',params.win, params.font);
Screen('TextSize',params.win, params.tsize);
Screen('TextStyle', params.win, 1);

%Maximum priority level
params.topPriorityLevel = MaxPriority(params.win);
Priority(params.topPriorityLevel);
%Query the frame duration
params.ifi = Screen('GetFlipInterval', params.win);
end


function PsychDefaultSetupPlus(featureLevel)
% PsychDefaultSetup(featureLevel) - Perform standard setup for Psychtoolbox.

% Default colormode to use: 0 = clamped, 0-255 range. 1 = unclamped 0-1 range.
global psych_default_colormode;
psych_default_colormode = 0;

% Reset KbName mappings:
clear KbName;

% Define maximum supported featureLevel for this Psychtoolbox installation:
maxFeatureLevel = 3;

% Sanity check featureLevel argument:
if nargin < 1 || isempty(featureLevel) || ~isscalar(featureLevel) || ~isnumeric(featureLevel) || featureLevel < 0
    error('Mandatory featureLevel argument missing or invalid (not a scalar number or negative).');
end

% Always AssertOpenGL:
AssertOpenGL;

% Level 1+ requested?
if featureLevel >= 1
    % Unify keycode to keyname mapping across operating systems:
    KbName('UnifyKeyNames');
end

% Level 2+ requested?
if featureLevel >= 2
    % Initial call to timing functions
    % Set global environment variable to ask PsychImaging() to enable
    % normalized color range for all drawing commands and Screen('MakeTexture'):
    psych_default_colormode = 1;
    GetSecs; WaitSecs(0.001);
end

% Level 2+ requested?
if featureLevel >= 3
    %suppress keypress to command window,
    %and hide the mouse pointer (usefull is most visual experiments)
    ListenChar(2);
    HideCursor;
end


if featureLevel > maxFeatureLevel
    error('This installation of Psychtoolbox can not execute scripts at the requested featureLevel of %i, but only up to level %i ! UpdatePsychtoolbox!', featureLevel, maxFeatureLevel);
end
return;
end


function startTime = ShowInstructions(params, instruct,keysToWaitFor)
if nargin < 3; keysToWaitFor = {'space', 'escape'}; end;
Screen('Flip',params.win); % for windows
DrawFormattedText(params.win, instruct, 'center', 'center',params.TextColor);
Screen('Flip',params.win);
RestrictKeysForKbCheck(cellfun(@KbName, keysToWaitFor));
deviceN = -1;
[startTime, keyCode] = KbWait(deviceN);
if strcmpi(KbName(keyCode), 'escape')
    CleanUp;
end
RestrictKeysForKbCheck([]);
Screen('Flip',params.win);
end


function CleanUp
ListenChar(0);
sca;
RestrictKeysForKbCheck([]);
%putFilesInDropbox(files);
end %CleanUp


function [targetWord, compWord, wordsMatch] = getTargetWordAndCompWord(words)
targetWordNum = ceil(rand*160);
targetWord = cell2mat(words(targetWordNum));
if rand > 0.33
    compWordNum=ceil(rand*9)+1;
    compWord = cell2mat(words(targetWordNum,compWordNum));
    wordsMatch = '2';
else
    compWord = targetWord;
    wordsMatch = '1';
end
end


function fixOnset = showFixationScreen(params, dur, keys)
RestrictKeysForKbCheck(cellfun(@KbName, keys));
DrawFormattedText(params.win, '+','center','center',params.colors.black,100);
fixOnset = Screen('Flip', params.win);
deviceN = -1;
[~, keyCode] = KbWait(deviceN, [], GetSecs + dur);
if strcmpi(KbName(keyCode), 'escape')
    CleanUp;
end
end


function imgOnset = showPicture(params, imgname, dur)
KbReleaseWait(-1); % so we don't catch any previous keys
%RestrictKeysForKbCheck(cellfun(@KbName, keys)); % only looks for keys we care about
img = imread(imgname); % read pic into memory
imgsize = size(img); % get pic size
params.picRect = [0 0 imgsize(2) imgsize(1)]; % get rect in order to center on screen
if params.picRect(3) > params.rect(3) & params.picRect(4) > params.rect(4) %#ok % if width and height of image are bigger than screen
   scaleFactor = params.rect(3)/params.picRect(3); % scale by the width factor
   img = imresize(img,scaleFactor);
   imgsize = size(img); % get pic size
end
params.picRect = [0 0 imgsize(2) imgsize(1)]; % get rect in order to center on screen
params.picRect = CenterRectOnPoint(params.picRect,params.Xc,params.Yc); % center on screen
Screen('Flip',params.win); % clear the screen
tex = Screen('MakeTexture', params.win, img); % make texture
Screen('DrawTexture', params.win, tex, [], [], 0); % draw the texture in back buffer
imgOnset = Screen('Flip', params.win);% flip image to screen
Screen('Flip', params.win,imgOnset + dur - (params.ifi/2)); % clear the screen
Screen('Close',tex);
end


function [rt, responseKey, videoStartTime] = PlayVideo(params, moviename, disableKeyPresses, keys)
if nargin < 3 | isempty(disableKeyPresses) %#ok
    disableKeyPresses = 0;
end
if nargin < 4
    keys = {};
end
RestrictKeysForKbCheck(cellfun(@KbName, keys)); % only looks for keys we care about
movie = Screen('OpenMovie', params.win, moviename); % Open movie file:
Screen('PlayMovie', movie, 1); % Start playback engine:
keyIsPressed = 0;
keyPressedTime = 0; %#ok
responseKey = [];
rt = [];
tstart = GetSecs;
videoStartTime = tstart;
while ~keyIsPressed
    tex = Screen('GetMovieImage', params.win, movie); % Wait for next movie frame, retrieve texture handle to it
    % Valid texture returned? A negative value means end of movie reached:
    if tex<=0
        break; % We're done, break out of loop:
    end
    Screen('DrawTexture', params.win, tex); % Draw the new texture immediately to screen:
    Screen('Flip', params.win); % Update display:
    Screen('Close', tex); % Release texture:
    if ~disableKeyPresses
        [keyIsPressed, keyPressedTime, keyCode] = KbCheck(-1);
        if keyIsPressed
            responseKey = KbName(keyCode);
            rt = keyPressedTime - tstart;
            Screen('Flip', params.win);
            if strcmpi(responseKey, 'escape') % if escape was pressed then exit the session
                Screen('PlayMovie', movie, 0);  % Stop playback:
                Screen('CloseMovie', movie);  % Close movie:
                CleanUp;
            end
        end
    end
end
Screen('PlayMovie', movie, 0);  % Stop playback:
Screen('CloseMovie', movie);  % Close movie:
end


function [responseKey, timeOfBtnPress] = showResponseScreen(params, respPrompt, dur, keys)
RestrictKeysForKbCheck(cellfun(@KbName, keys)); % only looks for keys we care about
timerStart = GetSecs; % start a timer to keep track of elapsed time
Screen('Flip',params.win); % clear the screen
trialElapsedTime = 0; % init elapsed time variable with zero value (will count up to dur)
keyIsPressed = 0; % init repsonse checker
responseKey = ''; % init response key
timeOfBtnPress = [];
% while a key hasn't been pressed, and the elapsed time is less than dur
while ~keyIsPressed & trialElapsedTime < dur %#ok
    [keyIsPressed, timeOfBtnPress, keyCode] = KbCheck(-1); % check for keyboard press
    DrawFormattedText(params.win,respPrompt,'center','center',params.colors.black);
    Screen('Flip', params.win);% flip image to screen
    if keyIsPressed % check if a response happened
        responseKey = KbName(keyCode);
        if strcmpi(responseKey, 'escape') % if escape was pressed then exit the session
            CleanUp;
        end
    end
    trialElapsedTime = GetSecs - timerStart; % keep track of elapsed time
end
Screen('Flip', params.win); % clear the screen
end


function isCorrect = checkIfTrialIsCorrect(responseKey,correctResponse)
if strcmpi(responseKey, correctResponse)
    isCorrect = 1;
elseif strcmpi(responseKey, '1!') & strcmpi(correctResponse, '1') %#ok
    isCorrect = 1;
elseif strcmpi(responseKey, '2@') & strcmpi(correctResponse, '2') %#ok
    isCorrect = 1;
elseif isempty(responseKey)
    isCorrect = -1;
else
    isCorrect = 0;
end
end

function fbOn = showFeedbackScreen(params, acc, dur)
posimg = fullfile(fileparts(mfilename('fullpath')),'txpics','good.bmp');
negimg = fullfile(fileparts(mfilename('fullpath')),'txpics','bad.bmp');
if acc == 1
    fbOn = showPicture(params, posimg, dur);
%     Screen('FillOval', params.win,[0 0.8 0], params.fbRect);
%     fbOn = Screen('Flip', params.win);
%     Screen('Flip', params.win, fbOn+dur);
    %DrawFormattedText(params.win, 'correct','center','center',[0 0.8 0]); % 'correct' in green color
elseif acc == 0
    fbOn = showPicture(params, negimg, dur);
%     Screen('FillOval', params.win,[0.8 0 0], params.fbRect);
%     fbOn = Screen('Flip', params.win);
%     Screen('Flip', params.win, fbOn+dur);
    %DrawFormattedText(params.win, 'wrong','center','center',[0.8 0 0]); % 'wrong' in red color
else
    DrawFormattedText(params.win, 'no response','center','center',[0 0 0]); % 'no response' in black color
    fbOn = Screen('Flip', params.win);
    Screen('Flip', params.win, fbOn+dur);
end

end

function foilType = getFoilType(word)
str = strsplit(word,'_');
typeStr = str{end};
if strfind(typeStr, 'S')
    foilType = 'S';
elseif strfind(typeStr, 'P')
    foilType = 'P';
elseif strfind(typeStr, 'U')
    foilType = 'U';
elseif strfind(typeStr, 'T')
    foilType = 'T';
end
end






